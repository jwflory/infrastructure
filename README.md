infrastructure
==============

[![License: BSD-2-Clause][1]][2]
[![Build Status][3]][4]

[Ansible][5]-based infrastructure configuration management


## About

This repository contains my configuration management for my personal web infrastructure.
It requires [Ansible][5].

There are also other scripts included here.
Most of them are Python or Bash scripts.
These are other tools I make use of to manage or automate the management of my own digital infrastructure.


## Legal

All code made available by [Justin W. Flory][6] under the [BSD-2-Clause][2] open source license.

[1]: https://img.shields.io/badge/License-BSD%202--Clause-orange.svg
[2]: https://opensource.org/licenses/BSD-2-Clause
[3]: https://travis-ci.org/jwflory/infrastructure.svg?branch=master
[4]: https://travis-ci.org/jwflory/infrastructure
[5]: https://www.ansible.com/
[6]: https://jwf.io
