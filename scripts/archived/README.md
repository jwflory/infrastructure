Archived scripts
================

This directory contains scripts no longer in use.
Perhaps they were replaced, or more likely, they are no longer needed.
They are kept for preservation purposes, but functionality is not guaranteed.

Use at your own risk.
