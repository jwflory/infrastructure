#!/bin/sh
#
# Barebones Asciidoctor docs deployment. Literally, barebones. I may explore
# other tooling to build on this prototype in the future.

# AsciiDoc base command (reusable in build script)
ASCIIDOC_CMD="asciidoctor --backend html5 --doctype=article"


# Always purge old site source and then start from docs source directory.
rm -rf public/
cd "$(dirname "$0")/docs/"


# Generate HTML pages for each AsciiDoc source file.
for doc in *; do
  if [ $doc != index.adoc ]; then
    echo $doc
    $ASCIIDOC_CMD \
      --out-file _build/$(echo $doc | sed --expression='s/.adoc//')/index.html \
      $doc
  else
    $ASCIIDOC_CMD \
      --out-file _build/index.html \
      $doc
  fi
done


# Move generated HTML pages to expected path by GitLab Pages CI.
mv _build/ ../public/
